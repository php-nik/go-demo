package controller

import (
	"gitlab.com/php-nik/go-demo/src/repository"
	"io"
	"net/http"
	"strconv"
)

type UserController interface {
	Hello(writer http.ResponseWriter, request *http.Request)
}

type userController struct {
	userRepository repository.UserRepository
}

func (u *userController) Hello(writer http.ResponseWriter, request *http.Request) {
	id, err := strconv.Atoi(request.URL.Query().Get("id"))
	if err != nil {
		id = 0
	}
	user := u.userRepository.GetUser(id)
	io.WriteString(writer, "Hello, "+user.Name)
}

func NewUserController(userRepository repository.UserRepository) UserController {
	return &userController{userRepository: userRepository}
}
