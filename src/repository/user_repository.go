package repository

import (
	"database/sql"
	"strconv"
)

type User struct {
	ID   int
	Name string
}

type UserRepository interface {
	GetUser(id int) *User
}

type userRepository struct {
	db *sql.DB
}

func (r *userRepository) GetUser(id int) *User {
	user := &User{ID: id, Name: "demo_user_" + strconv.Itoa(id)}

	return user
}

func NewUserRepository(db *sql.DB) UserRepository {
	return &userRepository{db: db}
}
