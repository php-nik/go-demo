module gitlab.com/php-nik/go-demo

go 1.22

require (
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
)
