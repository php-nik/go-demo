package main

import (
	"database/sql"
	"fmt"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"gitlab.com/php-nik/go-demo/src/controller"
	"gitlab.com/php-nik/go-demo/src/repository"
	"net/http"
	"os"
)

func main() {
	//Конфигурируем ENV VARS
	if "" == os.Getenv("APP_ENV") {
		os.Setenv("APP_ENV", "dev")
	}
	if "" == os.Getenv("APP_DEBUG") {
		os.Setenv("APP_DEBUG", "1")
	}
	appEnv := os.Getenv("APP_ENV")
	var appDebug bool
	if "1" == os.Getenv("APP_DEBUG") {
		appDebug = true
	} else {
		appDebug = false
	}
	godotenv.Load(".env." + appEnv + ".local")
	godotenv.Load(".env." + appEnv)
	godotenv.Load(".env.local")
	godotenv.Load(".env")

	fmt.Println(fmt.Sprintf("Run app (APP_ENV=%s, APP_DEBUG=%t)", appEnv, appDebug))

	// инициируем БД
	db, err := sql.Open("postgres", os.Getenv("DATABASE_URL"))
	if err != nil {
		panic(err)
	}
	defer db.Close()

	// проверяем коннект с БД
	err = db.Ping()
	if err != nil {
		panic(err)
	}

	// инициируем репозиторий
	userRepository := repository.NewUserRepository(db)

	// тут можем создать контроллеры/хендлеры и внедрить в них репозиторий
	userController := controller.NewUserController(userRepository)

	// тут создаем web-сервер, роутинг и указываем наши контроллеры/хэндлеры
	http.HandleFunc("/hello", userController.Hello)

	err = http.ListenAndServe(os.Getenv("HOST")+":"+os.Getenv("PORT"), nil)

	if err != nil {
		panic(err)
	}
}
