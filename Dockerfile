FROM golang:alpine AS build

# Устанавливаем текущую директорию = /build
WORKDIR /build

# Копируем только файлы, необходимые для установки зависимостей
COPY go.mod go.sum ./

# Устанавливаем зависимости
RUN go mod download

# Копируем все файлы из текущей директории на хост-машине в /build в образ
COPY . .

# Компилируем исполняемый файл для архитектуры linux/amd64 в файл /build/app
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app .

# Наследуемся от минималистичного образа Alpine чтобы не тащить в prod лишние зависимости
FROM alpine:latest

WORKDIR /app

# Копируем скомпилируемый исполняемый файл на предыдущем этапе
COPY --from=build /build/app /app/

CMD ["./app"]