
install:
	go mod download

build: install
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -o app .

run: build
	./app